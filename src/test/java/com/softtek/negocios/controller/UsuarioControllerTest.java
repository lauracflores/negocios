package com.softtek.negocios.controller;

import com.softtek.negocios.model.entity.Usuario;
import com.softtek.negocios.service.UsuarioService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringJUnit4ClassRunner.class)
public class UsuarioControllerTest {
    private MockMvc mockMvc;
    @Mock
    private UsuarioService usuarioService;
    @InjectMocks
    private UsuarioController usuarioController;
    @Before
    public void setUp()throws Exception{
        //crea el mock para luego testearlo
        mockMvc= MockMvcBuilders.standaloneSetup(usuarioController)
                .build();
    }
    @Test
    public void getUser() throws Exception{
        Usuario usuario =new Usuario();
        usuario.setId(1);
        usuario.setNombre("Edgar");
        Mockito.when(usuarioService.getUsuarioById(1)).thenReturn(new ResponseEntity(usuario, HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/users/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nombre", Matchers.is("Edgar")));

    }

    @Test
    public void getUsers() throws Exception{
        Usuario usuario =new Usuario();
        usuario.setId(1);
        usuario.setNombre("Edgar");
        List<Usuario> usuarios = new ArrayList<>();
        usuarios.add(usuario);
        Mockito.when(usuarioService.getAllUsuarios()).thenReturn(new ResponseEntity(usuarios, HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/usuarios")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]['nombre']", Matchers.is("Edgar")));
    }

    @Test
    public void setUser() throws Exception{
        Usuario usuario =new Usuario();
        usuario.setId(1);
        usuario.setNombre("Edgar");
        String json="{"+
                "\"id\": \"1\","+
                "\"nombre\": \"Edgar\""+
                "}";
        Mockito.when(usuarioService.setUsuario(usuario)).thenReturn(new ResponseEntity(usuario,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .characterEncoding("utf-8")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(print());
    }

    @Test
    public void updateUser() throws Exception{
        Usuario usuario =new Usuario();
        usuario.setId(1);
        usuario.setNombre("Edgar");
        String json="{"+
                "\"id\": \"1\","+
                "\"nombre\": \"Edgar\""+
                "}";
        Mockito.when(usuarioService.updateUsuario(1, usuario)).thenReturn(new ResponseEntity(usuario,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/users/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .characterEncoding("utf-8")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(print());
    }

    @Test
    public void deleteUser() throws Exception{
        Usuario usuario =new Usuario();
        usuario.setId(1);
        usuario.setNombre("Edgar");
        String json="{"+
                "\"id\": \"1\","+
                "\"nombre\": \"Edgar\""+
                "}";
                Mockito.when(usuarioService.deleteUsuario(1)).thenReturn(new ResponseEntity(usuario,HttpStatus.ACCEPTED));
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/users/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .characterEncoding("utf-8")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isAccepted())
                .andDo(print());
    }
}