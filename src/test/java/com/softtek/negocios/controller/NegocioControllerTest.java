package com.softtek.negocios.controller;


import com.softtek.negocios.model.entity.Negocio;
import com.softtek.negocios.service.NegocioService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.junit.runner.RunWith;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringJUnit4ClassRunner.class)
public class NegocioControllerTest {
    private MockMvc mockMvc;
    @Mock
    private NegocioService negocioService;
    @InjectMocks
    private NegocioController negocioController;

    @Before
    public void setUp()throws Exception{
        //crea el mock para luego testearlo
        mockMvc= MockMvcBuilders.standaloneSetup(negocioController)
                .build();
    }
    @Test
    public void getNegocio() throws Exception{
        Negocio negocio=new Negocio();
        negocio.setId(1);
        negocio.setNombre("edgar");
        Mockito.when(negocioService.getNegocioById(1)).thenReturn(new ResponseEntity(negocio,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/negocios/1")
                .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isOk())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.nombre", Matchers.is("edgar")));
    }

    @Test
    public void getNegocioByRazonSocial() throws Exception{
        Negocio negocio=new Negocio();
        negocio.setId(1);
        negocio.setRazonSocial("abarrotes");
        List<Negocio> negocios=new ArrayList<>();
        negocios.add(negocio);

        Mockito.when(negocioService.getNegocioByRazonSocial("abarrotes")).thenReturn(new ResponseEntity(negocios,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/negocios?razonSocial=abarrotes")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]['razonSocial']", Matchers.is("abarrotes")))
                .andReturn();

    }

    @Test
    public void getNegociobyNombre() throws Exception{
        Negocio negocio=new Negocio();
        negocio.setId(1);
        negocio.setRazonSocial("abarrotes");
        negocio.setNombre("Norma");
        List<Negocio> negocios=new ArrayList<>();
        negocios.add(negocio);

        Mockito.when(negocioService.getNegocioByNombre("Norma")).thenReturn(new ResponseEntity(negocios,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/negocios?nombre=Norma")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]['nombre']", Matchers.is("Norma")))
                .andReturn();
    }

    @Test
    public void getNegocios() throws Exception{
        Negocio negocio=new Negocio();
        negocio.setId(1);
        negocio.setRazonSocial("abarrotes");
        negocio.setNombre("Norma");
        List<Negocio> negocios=new ArrayList<>();
        negocios.add(negocio);

        Mockito.when(negocioService.getAllNegocios()).thenReturn(new ResponseEntity(negocios,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/negocios")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]['nombre']", Matchers.is("Norma")))
                .andReturn();
    }

    @Test
    public void setNegocio() throws Exception{
        Negocio negocio = new Negocio();
        negocio.setId(1);
        negocio.setNombre("ACTIVIDADES ADMINISTRATIVAS DE INSTITUCIONES DE BIENESTAR SOCIAL");
        String json="{"+
                "\"id\": \"1\","+
                "\"nombre\": \"ACTIVIDADES ADMINISTRATIVAS DE INSTITUCIONES DE BIENESTAR SOCIAL\","+
                "\"calle\": \"CALLE PRIVADA JAIME NUNÓ\","+
                "\"idUsuario\": \"1\""+
                "}";
        Mockito.when(negocioService.setNegocio(negocio)).thenReturn(new ResponseEntity(negocio,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.post("/negocios")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .characterEncoding("utf-8")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(print());
    }

    @Test
    public void updateNegocio() throws Exception {
        Negocio negocio = new Negocio();
        negocio.setId(1);
        negocio.setNombre("ACTIVIDADES ADMINISTRATIVAS DE INSTITUCIONES DE BIENESTAR SOCIAL");
        String json="{"+
                "\"id\": \"1\","+
                "\"nombre\": \"ACTIVIDADES ADMINISTRATIVAS DE INSTITUCIONES DE BIENESTAR SOCIAL\","+
                "\"calle\": \"CALLE PRIVADA JAIME NUNÓ\","+
                "\"idUsuario\": \"1\""+
                "}";
        Mockito.when(negocioService.updateNegocio(1,negocio)).thenReturn(new ResponseEntity(negocio,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/negocios/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .characterEncoding("utf-8")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(print());
    }

    @Test
    public void deleteNegocio() throws Exception{
        Negocio negocio = new Negocio();
        negocio.setId(1);
        negocio.setNombre("ACTIVIDADES ADMINISTRATIVAS DE INSTITUCIONES DE BIENESTAR SOCIAL");
        String json="{"+
                "\"id\": \"1\","+
                "\"nombre\": \"ACTIVIDADES ADMINISTRATIVAS DE INSTITUCIONES DE BIENESTAR SOCIAL\","+
                "\"calle\": \"CALLE PRIVADA JAIME NUNÓ\","+
                "\"idUsuario\": \"1\""+

                "}";
        Mockito.when(negocioService.deleteNegocio(1)).thenReturn(new ResponseEntity(negocio,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/negocios/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .characterEncoding("utf-8")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nombre", Matchers.is("ACTIVIDADES ADMINISTRATIVAS DE INSTITUCIONES DE BIENESTAR SOCIAL")))
                .andDo(print());
    }

    @Test
    public void get() throws Exception {
        Negocio negocio = new Negocio();
        negocio.setId(1);
        negocio.setNombre("ACTIVIDADES ADMINISTRATIVAS DE INSTITUCIONES DE BIENESTAR SOCIAL");
        negocio.setLongitud(23435);
        negocio.setLatitud(131241);
        List<Negocio> negocios=new ArrayList<>();
        negocios.add(negocio);
        Mockito.when(negocioService.getNegociosPorRadio(131241,23435,1)).thenReturn(new ResponseEntity(negocios,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/buscaNegocios?lat=131241&lng=23435&radio=1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]['nombre']", Matchers.is("ACTIVIDADES ADMINISTRATIVAS DE INSTITUCIONES DE BIENESTAR SOCIAL")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]['latitud']", Matchers.is(131241.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]['longitud']", Matchers.is(23435.0)))
                ;
    }
    @Test
    public void verificaNegocio() throws Exception{
        Negocio negocio=new Negocio();
        negocio.setId(1);
        negocio.setNombre("oxxo");
        List<Negocio> negocios=new ArrayList<>();
        negocios.add(negocio);

        Mockito.when(negocioService.verificaNegocio("oxxo",1,"aguascalientes")).thenReturn(new ResponseEntity(negocios,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/verificaNegocio?nombre=oxxo&entidad=1&municipio=aguascalientes")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]['nombre']", Matchers.is("oxxo")))
                .andReturn();
    }

    @Test
    public void verificaRegistroNegocio() throws Exception{
        Negocio negocio=new Negocio();
        negocio.setId(1);
        negocio.setIdDenue(1);
        negocio.setNombre("edgar");
        Mockito.when(negocioService.verificaRegistroNegocio(1)).thenReturn(new ResponseEntity(negocio,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/verificaRegistroNegocio?idDenue=1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.idDenue", Matchers.is(1)));

    }


}