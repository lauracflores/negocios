package com.softtek.negocios.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import com.softtek.negocios.model.entity.DiasAtencion;
import com.softtek.negocios.service.DiasAtencionService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.junit.runner.RunWith;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
public  class DiasAtencionControllerTest {
    private MockMvc mockMvc;
    @Mock
    private DiasAtencionService diasAtencionService;
    @InjectMocks
    private DiasAtencionController diasAtencionController;

    @Before
    public void setUp()throws Exception{
        //crea el mock para luego testearlo
        mockMvc= MockMvcBuilders.standaloneSetup(diasAtencionController)
                .build();
    }

    @Test
    public void getDiasAtencionByIdNegocio() throws Exception {
        DiasAtencion diaAtencion=new DiasAtencion();
        diaAtencion.setDia("Lunes");
        diaAtencion.setId(1);
        diaAtencion.setNegocio(1);
        List<DiasAtencion> diasAtencion=new ArrayList<>();
        diasAtencion.add(diaAtencion);
        Mockito.when(diasAtencionService.getDiasAtencionByNegocioId(1)).thenReturn(new ResponseEntity(diasAtencion,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/negocios/1/horario")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]['dia']", Matchers.is("Lunes")))
                .andReturn();
    }

    @Test
   public void getDiaAtencionById() throws Exception{
        DiasAtencion diaAtencion=new DiasAtencion();
        diaAtencion.setDia("Lunes");
        diaAtencion.setId(1);
        diaAtencion.setNegocio(1);
        Mockito.when(diasAtencionService.getDiasAtencionById(1)).thenReturn(new ResponseEntity(diaAtencion,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/horario/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.dia", Matchers.is("Lunes")))
                .andReturn();
    }

    @Test
    public void setDiaDeAtencion() throws Exception{
        DiasAtencion diaAtencion=new DiasAtencion();
        diaAtencion.setDia("Lunes");
        diaAtencion.setId(1);
        diaAtencion.setNegocio(1);
        String json="{"+
                "\"id\": \"1\","+
                "\"dia\": \"Lunes\","+
                "\"negocio\": \"1\""+
                "}";

        Mockito.when(diasAtencionService.setDiasAtencion(diaAtencion)).thenReturn(new ResponseEntity(diaAtencion,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.post("/horario")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .characterEncoding("utf-8")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void updateDiaDeAtencion() throws Exception {
        DiasAtencion diaAtencion=new DiasAtencion();
        diaAtencion.setDia("Lunes");
        diaAtencion.setId(1);
        diaAtencion.setNegocio(1);
        String json="{"+
                "\"id\": \"1\","+
                "\"dia\": \"Lunes\","+
                "\"negocio\": \"1\""+

                "}";

        Mockito.when(diasAtencionService.updateDiasAtencion(1,diaAtencion)).thenReturn(new ResponseEntity(diaAtencion,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/horario/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .characterEncoding("utf-8")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteDiaDeAtencion() throws Exception{
        DiasAtencion diaAtencion=new DiasAtencion();
        diaAtencion.setDia("Lunes");
        diaAtencion.setId(1);
        diaAtencion.setNegocio(1);
        String json="{"+
                "\"id\": \"1\","+
                "\"dia\": \"Lunes\","+
                "\"negocio\": \"1\""+

                "}";

        Mockito.when(diasAtencionService.deleteDiasAtencion(1)).thenReturn(new ResponseEntity(diaAtencion,HttpStatus.OK));
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/horario/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .characterEncoding("utf-8")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}