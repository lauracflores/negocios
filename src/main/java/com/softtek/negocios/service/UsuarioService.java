package com.softtek.negocios.service;

import com.softtek.negocios.model.entity.Usuario;
import com.softtek.negocios.model.entity.Response;
import com.softtek.negocios.model.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UsuarioService {
String mensajeError="Ocurrió un error.";
    @Autowired
    private UsuarioRepository usuarioRepository;

    public ResponseEntity getUsuarioById(int id) {
        try {
            Usuario usuario = usuarioRepository.findById(id);
            if (usuario == null) {
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND, "No se encontró un usuario con ese id: " + id), HttpStatus.NOT_FOUND);
            } else {
                usuario.setPassword("");
                return new ResponseEntity(usuario, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, mensajeError), HttpStatus.BAD_REQUEST);
        }
    }
    public ResponseEntity getUsuarioByUsuario(String nombreUsuario) {
        try {
            Usuario usuario = usuarioRepository.findByNombreUsuario(nombreUsuario);
            if (usuario == null) {
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND, "Usuario disponible"), HttpStatus.NOT_FOUND);
            } else
                return new ResponseEntity( usuario, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, mensajeError), HttpStatus.BAD_REQUEST);
        }
    }
    public ResponseEntity getUsuarioByCorreo(String email) {
        try {
            Usuario usuario = usuarioRepository.findByCorreo(email);
            if (usuario == null) {
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND, "Email disponible"), HttpStatus.NOT_FOUND);
            }
            else
                return new ResponseEntity( usuario, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, mensajeError), HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity getAllUsuarios(){
        try {
            List<Usuario> usuarios = usuarioRepository.findAll();
            if(usuarios.isEmpty()){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"No se encontró ningún usuario"), HttpStatus.NOT_FOUND);
            }else {
                for (Usuario usuario: usuarios
                     ) {
                    usuario.setPassword("");
                }
                return new ResponseEntity(usuarios, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,mensajeError), HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity setUsuario(Usuario usuario) {
        try {
            usuarioRepository.save(usuario);
        } catch (Exception e) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, mensajeError), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(usuario, HttpStatus.CREATED);
    }

    public ResponseEntity updateUsuario(int id, Usuario newUsuario) {
        Usuario oldUsuario = usuarioRepository.findById(id);
        if (oldUsuario == null) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.NOT_FOUND, "No se encontró un usuario con el id : " + id), HttpStatus.NOT_FOUND);
        }
        try {
            usuarioRepository.save(updateUsuario(oldUsuario, newUsuario));
        } catch (Exception e) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, "No se puede actualizar el usuario el id: " + id+", ocurrió un problema"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(
                new Response(
                        HttpStatus.ACCEPTED, "Se actualizó correctamente"), HttpStatus.ACCEPTED);
    }

    public ResponseEntity deleteUsuario(int id) {
        Usuario usuario = usuarioRepository.findById(id);
        if (usuario == null) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.NOT_FOUND, "No se encontró un usuario con el id : " + id), HttpStatus.NOT_FOUND);
        }
        try {
            usuarioRepository.delete(usuario);
        } catch (Exception e) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, "No se puede eliminar el usuario el id: " + id+", ocurrió un problema"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(
                new Response(
                        HttpStatus.ACCEPTED, "Se eliminó correctamente"), HttpStatus.ACCEPTED);
    }
    public boolean login(String nombreUsuario, String password) {
        Usuario usuario = usuarioRepository.findByNombreUsuario(nombreUsuario);
        if(usuario!=null && usuario.getPassword().contentEquals(password)) {
                return true;
        }
        return false;

    }
    private Usuario updateUsuario(Usuario oldUsuario, Usuario newUsuario) {
        if(newUsuario.getNombre() != null && !newUsuario.getNombre().equals(""))
            oldUsuario.setNombre(newUsuario.getNombre());
        if(newUsuario.getApellidoMaterno() != null && !newUsuario.getApellidoMaterno().equals(""))
            oldUsuario.setApellidoMaterno(newUsuario.getApellidoMaterno());
        if(newUsuario.getApellidoPaterno() != null && !newUsuario.getApellidoPaterno().equals(""))
            oldUsuario.setApellidoPaterno(newUsuario.getApellidoPaterno());
        if(newUsuario.getCorreo() != null && !newUsuario.getCorreo().equals(""))
            oldUsuario.setCorreo(newUsuario.getCorreo());
        if(newUsuario.getPassword() != null && !newUsuario.getPassword().equals(""))
            oldUsuario.setPassword(newUsuario.getPassword());
        if(newUsuario.getTelefono() != 0)
            oldUsuario.setTelefono(newUsuario.getTelefono());
        if(newUsuario.getNombreUsuario() != null && !newUsuario.getNombreUsuario().equals(""))
            oldUsuario.setNombreUsuario(newUsuario.getNombreUsuario());
        return oldUsuario;
    }

}
