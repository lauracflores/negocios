package com.softtek.negocios.service;

import com.softtek.negocios.model.entity.Inegi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class InegiService {
    @Value("${api.denue.uri}")
    private String apiDenueUri;
    @Value("${auth.token.inegi}")
    private String token;
    private final RestTemplate call = new RestTemplate();
    public ResponseEntity verificaNegocio(String nombre, int entidad, String municipio){
        String urlApiBusqueda= apiDenueUri + nombre + "/" + entidad + "/1/10/" + token;
        Inegi[] response;
        List<Inegi> negocios = new ArrayList<>();
        try{
            response = call.getForObject(urlApiBusqueda, Inegi[].class);
            for(int i=0; i < response.length; i++){
                String ubi = response[i].getUbicacion();
                String[] arrOfStr = ubi.split(",", 3);
                if(arrOfStr[1].toLowerCase().contains(municipio.toLowerCase())){
                    negocios.add(response[i]);
                }
            }
        }catch(Exception e){
            return new ResponseEntity("No se encontró ningún negocio con la información proporcionada, por favor verifica y vuelve a intentar ", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(negocios,HttpStatus.OK);
    }
}
