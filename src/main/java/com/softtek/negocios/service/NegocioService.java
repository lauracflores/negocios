package com.softtek.negocios.service;

import com.softtek.negocios.model.entity.Negocio;
import com.softtek.negocios.model.entity.NegocioResult;
import com.softtek.negocios.model.entity.Response;
import com.softtek.negocios.model.repository.DiasAtencionRepository;
import com.softtek.negocios.model.repository.NegocioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NegocioService {
    String mensajeError="Ocurrió un error.";
    @Autowired
    private NegocioRepository negocioRepository;
    @Autowired
    private DiasAtencionRepository diasAtencionRepository;
    @Autowired
    private InegiService inegiService;

    public ResponseEntity getNegocioByNombre(String nombre) {
        try {
            List<Negocio> negocios = negocioRepository.findByNombre(nombre);
            if(negocios.isEmpty()){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"No se encontraron coincidencias con ese nombre"), HttpStatus.NOT_FOUND);
            }else {
                for (Negocio negocio : negocios) {
                    negocio.setDiasAtencion(diasAtencionRepository.findByNegocio(negocio.getId()));
                }
                return new ResponseEntity(negocios, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,mensajeError), HttpStatus.BAD_REQUEST);
        }
    }
    public ResponseEntity getNegocioByUsuario(int usuario) {
        try {
            List<Negocio> negocios = negocioRepository.findByIdUsuario(usuario);
            if(negocios.isEmpty()){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"No se encontraron coincidencias con ese usuario"), HttpStatus.NOT_FOUND);
            }else {
                for (Negocio negocio : negocios) {
                    negocio.setDiasAtencion(diasAtencionRepository.findByNegocio(negocio.getId()));
                }
                return new ResponseEntity(negocios, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,mensajeError), HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity getNegocioByRazonSocial(String razonSocial) {
        try {
            List<Negocio> negocios = negocioRepository.findByRazonSocial(razonSocial);
            if(negocios.isEmpty()){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"No se encontraron coincidencias con esa razon social"), HttpStatus.NOT_FOUND);
            }else {
                for (Negocio negocio : negocios) {
                    negocio.setDiasAtencion(diasAtencionRepository.findByNegocio(negocio.getId()));
                }
                return new ResponseEntity(negocios, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,mensajeError), HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity getAllNegocios(){
        try {
            List<Negocio> negocios = negocioRepository.findAll();
            if(negocios.isEmpty()){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"No hay negocios registrados"), HttpStatus.NOT_FOUND);
            }else {
                for (Negocio negocio : negocios) {
                    negocio.setDiasAtencion(diasAtencionRepository.findByNegocio(negocio.getId()));
                }
                return new ResponseEntity(negocios, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,mensajeError), HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity getNegocioById(int id){
        try {
            Negocio negocio = negocioRepository.findById(id);
            if(negocio==null){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"No hay un negocio con ese id"), HttpStatus.NOT_FOUND);
            }else {
                negocio.setDiasAtencion(diasAtencionRepository.findByNegocio(id));
                return new ResponseEntity(negocio, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,mensajeError), HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity setNegocio(Negocio negocio) {
        try {
            negocioRepository.save(negocio);
        } catch (Exception e) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, "Ocurrio un error."), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(negocio,HttpStatus.CREATED);
    }

    public ResponseEntity updateNegocio(int id, Negocio newNegocio) {
        Negocio oldNegocio = negocioRepository.findById(id);
        if (oldNegocio == null) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.NOT_FOUND, "No se encontró un negocio con el id: " + id), HttpStatus.NOT_FOUND);
        }
        try {
            negocioRepository.save(updateNegocio(oldNegocio,newNegocio));
        } catch (Exception e) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, "No se actualizó el negocio con el id: " + id+", ocurrió un problema"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(
                new Response(
                        HttpStatus.ACCEPTED, "Negocio actualizado correctamente"), HttpStatus.ACCEPTED);
    }
    public ResponseEntity deleteNegocio(int id) {
        Negocio negocio = negocioRepository.findById(id);
        if (negocio == null) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.NOT_FOUND, "No se encontró un negocio con el id: " + id), HttpStatus.NOT_FOUND);
        }
        try {
            negocioRepository.delete(negocio);
        } catch (Exception e) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, "No se eliminó el negocio con el id: " + id+", ocurrió un problema"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(
                new Response(
                        HttpStatus.ACCEPTED, "Negocio eliminado correctamente"), HttpStatus.ACCEPTED);
    }

    public ResponseEntity getNegociosPorRadio(float latitud, float longitud, float radio){
        List<NegocioResult> negocioResults=negocioRepository.getNegocioPorRadio(latitud,longitud,radio);
        List<Negocio> negocios=new ArrayList<>();

        for (NegocioResult item: negocioResults
             ) {
            Negocio negocio = new Negocio();
            negocio.setId(item.getId_negocio());
            negocio.setNombre(item.getNombre());
            negocio.setRazonSocial(item.getRazon_social());
            negocio.setCalle(item.getCalle());
            negocio.setNumInt(item.getNumero_interior());
            negocio.setNumExt(item.getNumero_exterior());
            negocio.setColonia(item.getColonia());
            negocio.setCodigoPostal(item.getCodigo_postal());
            negocio.setEstado(item.getEstado());
            negocio.setMunicipio(item.getMunicipio());
            negocio.setTelefono(item.getTelefono());
            negocio.setEmail(item.getEmail());
            negocio.setSitioWeb(item.getSitio_web());
            negocio.setLongitud(item.getLongitud());
            negocio.setLatitud(item.getLatitud());
            negocio.setDescripcion(item.getDescripcion_negocio());
            negocio.setDiasAtencion(diasAtencionRepository.findByNegocio(item.getId_negocio()));
            negocios.add(negocio);
            
        }

        return new ResponseEntity<>(negocios,HttpStatus.ACCEPTED);
    }

    public ResponseEntity verificaNegocio(String nombre, int entidad, String municipio){
        ResponseEntity resp = inegiService.verificaNegocio(nombre,entidad,municipio);
        if(resp.getStatusCode().equals(HttpStatus.NOT_FOUND)){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.NOT_FOUND, resp.getBody().toString()), HttpStatus.NOT_FOUND);
        }
        return resp;
    }

    public ResponseEntity verificaRegistroNegocio(int idDenue){
        try {
            Negocio negocio = negocioRepository.findByIdDenue(idDenue);
            if(negocio==null){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"No hay negocio con id_denue: "+idDenue), HttpStatus.NOT_FOUND);
            }else
                return new ResponseEntity(negocio, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,mensajeError), HttpStatus.BAD_REQUEST);
        }
    }
    private Negocio updateNegocio(Negocio oldNegocio, Negocio newNegocio) {
        if(newNegocio.getEmail() != null && !newNegocio.getEmail().equals(""))
            oldNegocio.setEmail(newNegocio.getEmail());
        if(newNegocio.getTelefono() != 0)
            oldNegocio.setTelefono(newNegocio.getTelefono());
        return oldNegocio;
    }

}
