package com.softtek.negocios.service;

import com.softtek.negocios.model.entity.DiasAtencion;
import com.softtek.negocios.model.entity.Response;
import com.softtek.negocios.model.repository.DiasAtencionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DiasAtencionService {

    @Autowired
    private DiasAtencionRepository diasAtencionRepository;

    public ResponseEntity getDiasAtencionById(int id){
        try {
            DiasAtencion diasAtencion = diasAtencionRepository.findById(id);
            if(diasAtencion==null){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"No hay un dia de atencion con ese id"), HttpStatus.NOT_FOUND);
            }else
                return new ResponseEntity(diasAtencion, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,"Ocurrió un error"), HttpStatus.BAD_REQUEST);
        }
    }
    public ResponseEntity getDiasAtencionByNegocioId(int id){
        try {
            List<DiasAtencion> diasAtencion = diasAtencionRepository.findByNegocio(id);
            if(diasAtencion.isEmpty()){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"No hay un horario registrado para ese negocio"), HttpStatus.NOT_FOUND);
            }else
                return new ResponseEntity(diasAtencion, HttpStatus.OK);
       }catch (Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,"Ocurrió un error"), HttpStatus.BAD_REQUEST);
        }
    }
    public ResponseEntity setDiasAtencion(DiasAtencion diasAtencion) {
        try {
            diasAtencionRepository.save(diasAtencion);
        } catch (Exception e) {
            return new ResponseEntity(
                    new Response(
                            HttpStatus.BAD_REQUEST, "Ocurrio un error."), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(diasAtencion, HttpStatus.CREATED);
    }
    public ResponseEntity updateDiasAtencion(int id, DiasAtencion newDiasAtencion) {
        DiasAtencion oldDiasAtencion = diasAtencionRepository.findById(id);
        if (oldDiasAtencion == null) {
            return new ResponseEntity(
                    new Response(
                            HttpStatus.NOT_FOUND, "No se encontró un dia de atención con el id: " + id), HttpStatus.NOT_FOUND);
        }
        try {
            diasAtencionRepository.save(updateDiasAtencion(oldDiasAtencion, newDiasAtencion));
        } catch (Exception e) {
            return new ResponseEntity(
                    new Response(
                            HttpStatus.BAD_REQUEST, "No se actualizó el día de atención con el id: " + id+", ocurrió un problema"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(
                new Response(
                        HttpStatus.ACCEPTED, "día de atención actualizado correctamente"), HttpStatus.ACCEPTED);
    }

    public ResponseEntity deleteDiasAtencion(int id) {
        DiasAtencion diasAtencion = diasAtencionRepository.findById(id);
        if (diasAtencion == null) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.NOT_FOUND, "No se encontró un día de atención con el id: " + id), HttpStatus.NOT_FOUND);
        }
        try {
            diasAtencionRepository.delete(diasAtencion);
        } catch (Exception e) {
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, "No se eliminó el día de atención con el id: " + id+", ocurrió un problema"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(
                new Response(
                        HttpStatus.ACCEPTED, "día de atención eliminado correctamente"), HttpStatus.ACCEPTED);
    }
    private DiasAtencion updateDiasAtencion(DiasAtencion oldDiaAtencion, DiasAtencion newDiaAtencion) {
            oldDiaAtencion.setAbierto(newDiaAtencion.getAbierto());
            oldDiaAtencion.setHoraApertura(newDiaAtencion.getHoraApertura());
            oldDiaAtencion.setHoraCierre(newDiaAtencion.getHoraCierre());
        if(newDiaAtencion.getDia() != null && !newDiaAtencion.getDia().equals(""))
            oldDiaAtencion.setDia(newDiaAtencion.getDia());
        if(newDiaAtencion.getNegocio() != 0)
            oldDiaAtencion.setNegocio(newDiaAtencion.getNegocio());

        return oldDiaAtencion;
    }
}