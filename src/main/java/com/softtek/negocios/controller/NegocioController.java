package com.softtek.negocios.controller;

import com.softtek.negocios.model.dto.NegocioDTO;
import com.softtek.negocios.model.entity.Negocio;
import com.softtek.negocios.service.DiasAtencionService;
import com.softtek.negocios.service.NegocioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
@CrossOrigin(origins = "${cors.front.uri}", maxAge = 3600)
@RestController
public class NegocioController {

    @Autowired
    private NegocioService negocioService;

    @Autowired
    DiasAtencionService diasAtencionService;

    @GetMapping(value = "/negocios/{id}")
    public ResponseEntity getNegocio(@PathVariable(value = "id") int id){
        return negocioService.getNegocioById(id);
    }

    @GetMapping(value = "/negocios", params="razonSocial")
    public ResponseEntity getNegocioByRazonSocial(@RequestParam(value = "razonSocial") String razonSocial){ return negocioService.getNegocioByRazonSocial(razonSocial); }

    @GetMapping(value = "/negocios", params="nombre")
    public ResponseEntity getNegociobyNombre(@RequestParam(value = "nombre") String nombre){ return negocioService.getNegocioByNombre(nombre); }

    @GetMapping(value = "/negocios", params="usuario")
    public ResponseEntity getNegociobyUsuario(@RequestParam(value = "usuario") int usuario){ return negocioService.getNegocioByUsuario(usuario); }


    @GetMapping(value = "/negocios")
    public ResponseEntity getNegocios(){
        return negocioService.getAllNegocios();
    }

    @PostMapping(value = "/negocios", headers = "Accept=application/json")
    public ResponseEntity setNegocio(@Valid @RequestBody NegocioDTO negociodto){
        Negocio negocio=new Negocio();
            negocio.setId(negociodto.getId());
            negocio.setIdDenue(negociodto.getIdDenue());
            negocio.setNombre(negociodto.getNombre());
            negocio.setRazonSocial(negociodto.getRazonSocial());
            negocio.setCalle(negociodto.getCalle());
            negocio.setNumInt(negociodto.getNumInt());
            negocio.setNumExt(negociodto.getNumExt());
            negocio.setColonia(negociodto.getColonia());
            negocio.setCodigoPostal(negociodto.getCodigoPostal());
            negocio.setEstado(negociodto.getEstado());
            negocio.setMunicipio(negociodto.getMunicipio());
            negocio.setTelefono(negociodto.getTelefono());
            negocio.setEmail(negociodto.getEmail());
            negocio.setSitioWeb(negociodto.getSitioWeb());
            negocio.setLongitud(negociodto.getLongitud());
            negocio.setLatitud(negociodto.getLatitud());
            negocio.setDescripcion(negociodto.getDescripcion());
            negocio.setIdUsuario(negociodto.getIdUsuario());
        return negocioService.setNegocio(negocio);
    }
    @PutMapping(value = "/negocios/{id}")
    public ResponseEntity updateNegocio(@PathVariable int id, @Valid @RequestBody NegocioDTO negociodto){
        Negocio negocio=new Negocio();
        negocio.setTelefono(negociodto.getTelefono());
        negocio.setEmail(negociodto.getEmail());
                return negocioService.updateNegocio(id, negocio);
    }

    @DeleteMapping(value = "/negocios/{id}")
    public ResponseEntity deleteNegocio(@PathVariable int id){ return negocioService.deleteNegocio(id); }

    @GetMapping(value = "/buscaNegocios", params = { "lat","lng","radio" })
    public ResponseEntity get(@RequestParam(value = "lat") float latitud, @RequestParam(value = "lng") float longitud, @RequestParam(value = "radio") float radio){ return negocioService.getNegociosPorRadio(latitud,longitud,radio); }

    @GetMapping(value = "/verificaNegocio", params = {"nombre","entidad","municipio"})
    public ResponseEntity verificaNegocio(@RequestParam(value = "nombre") String nombre, @RequestParam(value = "entidad") int entidad, @RequestParam(value = "municipio") String municipio){ return negocioService.verificaNegocio(nombre, entidad,municipio); }

    @GetMapping(value = "/verificaRegistroNegocio", params = "idDenue")
    public ResponseEntity verificaRegistroNegocio(@RequestParam()int idDenue){ return negocioService.verificaRegistroNegocio(idDenue); }

}
