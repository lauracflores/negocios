package com.softtek.negocios.controller;

import com.softtek.negocios.model.dto.UsuarioDTO;
import com.softtek.negocios.model.entity.Usuario;
import com.softtek.negocios.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.*;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "${cors.front.uri}", maxAge = 3600)
@RestController
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping(value = "/usuarios/{id}")
    public ResponseEntity getUsuario(@PathVariable(value = "id") int id){
        return usuarioService.getUsuarioById(id);
    }
    @GetMapping(value = "/usuarios", params="usuario")
    public ResponseEntity getUsuarioByUsuario(@RequestParam(value = "usuario") String usuario){
        return usuarioService.getUsuarioByUsuario(usuario);
    }
    @GetMapping(value = "/usuarios", params="email")
    public ResponseEntity getUsuarioByEmail(@RequestParam(value = "email") String email){
        return usuarioService.getUsuarioByCorreo(email);
    }

    @GetMapping(value = "/usuarios")
    public ResponseEntity getUsuarios(){ return usuarioService.getAllUsuarios(); }

    @PostMapping(value = "/usuarios", headers = "Accept=application/json")
    public ResponseEntity setUsuario(@Valid @RequestBody UsuarioDTO usuariodto){
        Usuario usuario= new Usuario();
        usuario.setId(usuariodto.getId());
        usuario.setNombre(usuariodto.getNombre());
        usuario.setApellidoPaterno(usuariodto.getApellidoPaterno());
        usuario.setApellidoMaterno(usuariodto.getApellidoMaterno());
        usuario.setCorreo(usuariodto.getCorreo());
        usuario.setNombreUsuario(usuariodto.getNombreUsuario());
        usuario.setTelefono(usuariodto.getTelefono());
        usuario.setPassword(usuariodto.getPassword());
        return usuarioService.setUsuario(usuario);
    }

    @PutMapping(value = "/usuarios/{id}")
    public ResponseEntity updateUsuario(@PathVariable int id, @Valid @RequestBody UsuarioDTO usuariodto){
        Usuario usuario= new Usuario();
        usuario.setId(usuariodto.getId());
        usuario.setNombre(usuariodto.getNombre());
        usuario.setApellidoPaterno(usuariodto.getApellidoPaterno());
        usuario.setApellidoMaterno(usuariodto.getApellidoMaterno());
        usuario.setCorreo(usuariodto.getCorreo());
        usuario.setNombreUsuario(usuariodto.getNombreUsuario());
        usuario.setTelefono(usuariodto.getTelefono());
        usuario.setPassword(usuariodto.getPassword());
        return usuarioService.updateUsuario(id, usuario); }

    @DeleteMapping(value = "/usuarios/{id}")
    public ResponseEntity deleteUsuario(@PathVariable int id){ return usuarioService.deleteUsuario(id); }

    @PostMapping(value= "/login")
    public ResponseEntity login(@RequestParam("usuario") String username, @RequestParam("password") String pwd) {
        if(usuarioService.login(username,pwd)) {
            String token = getJWTToken(username);
            Usuario usuario = new Usuario();
            usuario.setNombreUsuario(username);
            usuario.setToken(token);
            return new ResponseEntity(usuario, HttpStatus.OK);
        }
        else{

            return new ResponseEntity("Usuario o contraseña incorrectos", HttpStatus.NOT_FOUND);
        }

    }

    private String getJWTToken(String username) {
        String secretKey = "mySecretKey";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("USUARIO-ROL");

        String token = Jwts
                .builder()
                .setId("covidJWT")
                .setSubject(username)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1800000))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return "Bearer " + token;
    }

}
