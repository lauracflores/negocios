package com.softtek.negocios.controller;

import com.softtek.negocios.model.dto.DiasAtencionDTO;
import com.softtek.negocios.model.entity.DiasAtencion;
import com.softtek.negocios.service.DiasAtencionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
@CrossOrigin(origins = "${cors.front.uri}", maxAge = 3600)
@RestController
public class DiasAtencionController {

    @Autowired
    private DiasAtencionService diasAtencionService;

    @GetMapping(value = "/negocios/{idNegocio}/horario")
    public ResponseEntity getDiasAtencionByIdNegocio(@PathVariable(value = "idNegocio") int idNegocio){ return diasAtencionService.getDiasAtencionByNegocioId(idNegocio); }

    @GetMapping(value = "/diasAtencion/{id}")
    public ResponseEntity getDiaAtencionById(@PathVariable(value = "id") int id){ return diasAtencionService.getDiasAtencionById(id); }

    @PostMapping(value = "/diasAtencion", headers = "Accept=application/json")
    public ResponseEntity setDiaDeAtencion(@Valid @RequestBody DiasAtencionDTO diasAtenciondto){
        DiasAtencion diasAtencion = new DiasAtencion();
        diasAtencion.setId(diasAtenciondto.getId());
        diasAtencion.setDia(diasAtenciondto.getDia());
        diasAtencion.setAbierto(diasAtenciondto.getAbierto());
        diasAtencion.setHoraApertura(diasAtenciondto.getHoraApertura());
        diasAtencion.setHoraCierre(diasAtenciondto.getHoraCierre());
        diasAtencion.setNegocio(diasAtenciondto.getNegocio());
        return diasAtencionService.setDiasAtencion(diasAtencion); }

    @PutMapping(value = "/diasAtencion/{id}")
    public ResponseEntity updateDiaDeAtencion(@PathVariable int id, @Valid @RequestBody DiasAtencionDTO diasAtenciondto){
        DiasAtencion diasAtencion = new DiasAtencion();
        diasAtencion.setId(diasAtenciondto.getId());
        diasAtencion.setDia(diasAtenciondto.getDia());
        diasAtencion.setAbierto(diasAtenciondto.getAbierto());
        diasAtencion.setHoraApertura(diasAtenciondto.getHoraApertura());
        diasAtencion.setHoraCierre(diasAtenciondto.getHoraCierre());
        diasAtencion.setNegocio(diasAtenciondto.getNegocio());
        return diasAtencionService.updateDiasAtencion(id, diasAtencion); }

    @DeleteMapping(value = "/diasAtencion/{id}")
    public ResponseEntity deleteDiaDeAtencion(@PathVariable int id){ return diasAtencionService.deleteDiasAtencion(id); }

}
