package com.softtek.negocios.model.repository;

import com.softtek.negocios.model.entity.Negocio;
import com.softtek.negocios.model.entity.NegocioResult;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NegocioRepository extends org.springframework.data.jpa.repository.JpaRepository<Negocio, Long>  {
    @Query("SELECT n FROM Negocio n WHERE n.nombre LIKE %:nombre%")
    public List<Negocio> findByNombre(@Param("nombre")String nombre);
    @Query("SELECT n FROM Negocio n WHERE n.razonSocial LIKE %:razonsocial%")
    public List<Negocio> findByRazonSocial(@Param("razonsocial") String razonSocial);
    public List<Negocio> findAll();
    public Negocio findById(int id);
    public List<Negocio> findByIdUsuario(int usuario);

    @Query(value = "call busca_negocios_por_radio( :lat, :lng, :rango );", nativeQuery = true)
    List<NegocioResult> getNegocioPorRadio(@Param("lat") float lat , @Param("lng") float lng , @Param("rango")  float rango);

    @Query("SELECT n FROM Negocio n WHERE n.idDenue = :id")
    Negocio findByIdDenue(int id);

}
