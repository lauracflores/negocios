package com.softtek.negocios.model.repository;

import com.softtek.negocios.model.entity.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends CrudRepository <Usuario, Integer> {

    @Query("SELECT s FROM Usuario s WHERE s.id = ?1")
    Usuario findById(@Param("id") int id);

    @Query("SELECT s FROM Usuario s WHERE s.id > 0")
    List<Usuario> findAll();

    public Usuario findByNombreUsuario(String nombreUsuario);

    public Usuario findByCorreo(String email);




}
