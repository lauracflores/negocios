package com.softtek.negocios.model.repository;

import com.softtek.negocios.model.entity.DiasAtencion;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface DiasAtencionRepository extends org.springframework.data.jpa.repository.JpaRepository<DiasAtencion, Long>  {
    public DiasAtencion findById(int id);
    public List<DiasAtencion> findByNegocio(int id);
}
