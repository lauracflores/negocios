package com.softtek.negocios.model.dto;

import org.apache.tomcat.jni.Local;

import java.time.LocalTime;

public class DiasAtencionDTO {
    private int id;
    private String dia;
    private int abierto;
    private LocalTime horaApertura;
    private LocalTime horaCierre;
    private int negocio;

    public int getId() {
        return id;
    }

    public String getDia() {
        return dia;
    }

    public int getAbierto() {
        return abierto;
    }

    public LocalTime getHoraApertura() {
        return horaApertura;
    }

    public LocalTime getHoraCierre() {
        return horaCierre;
    }

    public int getNegocio() {
        return negocio;
    }
}
