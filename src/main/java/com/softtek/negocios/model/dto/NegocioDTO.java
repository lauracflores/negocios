package com.softtek.negocios.model.dto;

public class NegocioDTO {
    private int id;
    private Integer idDenue;
    private String nombre;
    private String razonSocial;
    private String calle;
    private String numInt;
    private String numExt;
    private String colonia;
    private int codigoPostal;
    private String estado;
    private String municipio;
    private long telefono;
    private String email;
    private String sitioWeb;
    private float longitud;
    private float latitud;
    private String descripcion;
    private int idUsuario;

    public int getId() {
        return id;
    }

    public Integer getIdDenue() {
        return idDenue;
    }

    public String getNombre() {
        return nombre;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public String getCalle() {
        return calle;
    }

    public String getNumInt() {
        return numInt;
    }

    public String getNumExt() {
        return numExt;
    }

    public String getColonia() {
        return colonia;
    }

    public int getCodigoPostal() {
        return codigoPostal;
    }

    public String getEstado() {
        return estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public long getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public String getSitioWeb() {
        return sitioWeb;
    }

    public float getLongitud() {
        return longitud;
    }

    public float getLatitud() {
        return latitud;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getIdUsuario() {
        return idUsuario;
    }
}
