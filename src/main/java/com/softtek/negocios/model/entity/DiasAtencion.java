package com.softtek.negocios.model.entity;

import javax.persistence.*;
import java.time.LocalTime;
@Entity
@Table(name="diasatencion")
public class DiasAtencion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_dia_atencion")
    private int id;
    @Column(name="dia")
    private String dia;
    @Column(name="abierto")
    private int abierto;
    @Column(name="hora_apertura")
    private LocalTime horaApertura;
    @Column(name="hora_cierre")
    private LocalTime horaCierre;
    @Column(name="id_negocio")
    private int negocio;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public int getAbierto() {
        return abierto;
    }

    public void setAbierto(int abierto) {
        this.abierto = abierto;
    }

    public LocalTime getHoraApertura() {
        return horaApertura;
    }

    public void setHoraApertura(LocalTime horaApertura) {
        this.horaApertura = horaApertura;
    }

    public LocalTime getHoraCierre() {
        return horaCierre;
    }

    public void setHoraCierre(LocalTime horaCierre) {
        this.horaCierre = horaCierre;
    }

    public int getNegocio() {
        return negocio;
    }

    public void setNegocio(int negocio) {
        this.negocio = negocio;
    }

}
