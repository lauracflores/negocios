package com.softtek.negocios.model.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="negocio")
public class Negocio {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_negocio")
    private int id;
    @Column(name = "id_denue")
    private Integer idDenue;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "razon_social")
    private String razonSocial;
    @Column(name = "calle")
    private String calle;
    @Column(name = "numero_interior")
    private String numInt;
    @Column(name = "numero_exterior")
    private String numExt;
    @Column(name = "colonia")
    private String colonia;
    @Column(name = "codigo_postal")
    private int codigoPostal;
    @Column(name = "estado")
    private String estado;
    @Column(name = "municipio")
    private String municipio;
    @Column(name = "telefono")
    private long telefono;
    @Column(name = "email")
    private String email;
    @Column(name = "sitio_web")
    private String sitioWeb;
    @Column(name = "longitud")
    private double longitud;
    @Column(name = "latitud")
    private double latitud;
    @Column(name = "descripcion_negocio")
    private String descripcion;
    @Column(name = "id_usuario")
    private int idUsuario;
    @Transient
    private List<DiasAtencion> diasAtencion;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdDenue() {
        return idDenue;
    }

    public void setIdDenue(Integer idDenue) {
        this.idDenue = idDenue;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumInt() {
        return numInt;
    }

    public void setNumInt(String numInt) {
        this.numInt = numInt;
    }

    public String getNumExt() {
        return numExt;
    }

    public void setNumExt(String numExt) {
        this.numExt = numExt;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public int getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSitioWeb() {
        return sitioWeb;
    }

    public void setSitioWeb(String sitioWeb) {
        this.sitioWeb = sitioWeb;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public List<DiasAtencion> getDiasAtencion() {
        return diasAtencion;
    }

    public void setDiasAtencion(List<DiasAtencion> diasAtencion) {
        this.diasAtencion = diasAtencion;
    }
}