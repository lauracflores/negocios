package com.softtek.negocios.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Inegi {
    @JsonProperty("Id")
    private String id;
    @JsonProperty("Nombre")
    private String nombre;
    @JsonProperty("Razon_social")
    private String razonSocial;
    @JsonProperty("Clase_actividad")
    private String claseActividad;
    @JsonProperty("Calle")
    private String calle;
    @JsonProperty("Num_Exterior")
    private String numExt;
    @JsonProperty("Num_Interior")
    private String numInt;
    @JsonProperty("Colonia")
    private String colonia;
    @JsonProperty("CP")
    private String codigoPostal;
    @JsonProperty("Ubicacion")
    private String ubicacion;
    @JsonProperty("Telefono")
    private long telefono;
    @JsonProperty("Correo_e")
    private String email;
    @JsonProperty("Sitio_internet")
    private String sitioWeb;
    @JsonProperty("Longitud")
    private double longitud;
    @JsonProperty("Latitud")
    private double latitud;

    public String getUbicacion() {
        return ubicacion;
    }
}