package com.softtek.negocios.model.entity;



public interface NegocioResult {
    int getId_negocio();
    String getNombre();
    String getRazon_social();
    double getLatitud();
    double getLongitud();
    String getSitio_web();
    String getEmail();
    String getCalle();
    String getNumero_exterior();
    String getNumero_interior();
    String getColonia();
    String getMunicipio();
    String getEstado();
    int getCodigo_postal();
    long getTelefono();
    String getDescripcion_negocio();
    double getDistance();

}
